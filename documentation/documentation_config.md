---
project: Fortran Memory Available
project_gitlab: https://gitlab.com/fortran_utilities/fortran_memory_available
project_download: https://gitlab.com/fortran_utilities/fortran_memory_available/-/archive/main/fortran_memory_available-main.tar.gz
summary:
	A small toy fortran project to try to work out how much memory can be
	allocated at run time.
author: D Dickinson
author_description:
	Computational physicist based at the University of York
parallel: 16
src_dir: ../src
css: user.css
output_dir: ./html
graph_dir: ./graphs
page_dir: ./pages
media_dir: ./media
fpp_extensions: F90
predocmark: >
predocmark_alt: !
docmark: <
docmark_alt: #
display: public
display: protected
display: private
source: true
print_creation_date: true
search: false
graph: false
coloured_edges: true
---

{!../README.md!}
