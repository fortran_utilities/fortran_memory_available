# Adds target for pfunit tests in test_source
# name fma_tests_${test_name} and adds to
# list of know tests cases to give to ctest.
function(fma_add_test test_source test_name)
  add_pfunit_test(fma_tests_${test_name}
    "${test_source}" "" "")
  target_link_libraries(fma_tests_${test_name} fortran_memory_available)
  list(APPEND FMA_CTEST_CASES fma_tests_${test_name})
  set(FMA_CTEST_CASES ${FD_CTEST_CASES} PARENT_SCOPE)
endfunction()

# Helper function to easily add multiple separate
# tests provided they exist at tests/test_${name}.pf
# and we're happy to identify them as fma_tests_${name}
function(fma_add_tests)
  cmake_parse_arguments(
	FMA_ADD "" "" "TEST_NAMES" ${ARGN}
    )
  foreach(name ${FMA_ADD_TEST_NAMES})
    fma_add_test("test_${name}.pf" ${name})
  endforeach()
  set(FMA_CTEST_CASES ${FMA_CTEST_CASES} PARENT_SCOPE)
endfunction()
